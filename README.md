# Интеграция 1С и Apache Kafka (Rest)
Используется для отправки и получения данных из брокера сообщений Apache Kafka с помощью Confluent REST Proxy (https://docs.confluent.io/platform/current/kafka-rest/index.html).

### Программный интерфейс 1C представлен методами:
- ИнтеграцияКафкаСервер.ОтправитьСообщенияКафка(..)
- ИнтеграцияКафкаСервер.ПолучитьСообщенияКафка(...)

### Запуск Kafka в Docker:
```
docker-compose -f dockers/docker-compose.yml up -d
```
Подробнее на https://docs.confluent.io/platform/current/quickstart/ce-docker-quickstart.html#ce-docker-quickstart
